#include<stdio.h>

int binary_search(long int a[], long int n, long int key)
	{
		long int i, start, end, mid;

		start  = 0;
		end   = n - 1;
	    mid = (start + end)/2;

	    if(a[start]<=a[end]){
            while( start <= end && a[mid] != key )
            {
              if ( a[mid] > key )
                  end = mid - 1;
              else
                 start = mid + 1;

              mid = (start + end)/2;
           }
           if ( start > end )
		       return -1;
	    }

	   else{
            while( start <= end && a[mid] != key )
            {
              if ( a[mid] > key )
                 start = mid + 1;
              else
                 end = mid - 1;

              mid = (start + end)/2;
           }
           if ( start > end )
		       return -1;
	   }

	   return mid;

	}

int main()
{
    int T, i;
    scanf("%d", &T);

    for(i=1; i<=T; ++i){
        long int N, j, p, temp;
        scanf("%ld", &N);
        long int n[N];

        for(j=0; j<N; ++j){
            scanf("%ld", &n[j]);
        }
        scanf("%ld", &p);

        temp = binary_search(n, N, p);

        if(temp > -1)
            printf("Case %d: Found.\n", i);
        else
            printf("Case %d: Not Found.\n", i);

    }
    return 0;
}
